package com.zuitt.activity1;
import java.util.Scanner;

public class Activity1 {
    public static void main(String[] args) {

        String firstName, lastName;
        Double firstSubject, secondSubject, thirdSubject;

        Scanner myObj = new Scanner(System.in);

        System.out.println("First Name: ");
        firstName = new String(myObj.nextLine());
        System.out.println("Last Name: ");
        lastName = new String(myObj.nextLine());
        System.out.println("First Subject Grade: ");
        firstSubject = new Double(myObj.nextLine());
        System.out.println("Second Subject Grade: ");
        secondSubject = new Double(myObj.nextLine());
        System.out.println("Third Subject Grade: ");
        thirdSubject = new Double(myObj.nextLine());

        System.out.println("Good daY! " + firstName + " " + lastName + ".");
        System.out.println("Your grade average is: " + Math.round((firstSubject + secondSubject + thirdSubject) / 3));

    }
}
